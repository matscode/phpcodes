<?php
	/**
	 *
	 * Description
	 *
	 * @package        learnphp.vcom
	 * @category       Source
	 * @author         Michael Akanji <matscode@gmail.com>
	 * @link           https://www.michaelakanji.com
	 * @date           2017-06-15
	 * @copyright (c)  2016 - 2017, TECRUM (http://www.tecrum.com)
	 *
	 */


	/**
	 * -----------------------------
	 * Creating a function to generate stars
	 * ----------------------------------------
	 */

	/**
	 * @param integer $numberOfStar  Number of stars you want to print on a line
	 * @param string  $starCharacter Default is '*' Can be replaced with desired character
	 */
	function generateStar( $numberOfStar, $starCharacter = '* ' )
	{
		for (
			$starCount = 1; // first star count
			$starCount <= $numberOfStar; // checking star count if in range if numberOfStar
			$starCount ++ // increase start count
		) {
			echo $starCharacter;
		}
	}


	/**
	 * ----------------------------------
	 * Creating a function to generate Non breaking space
	 * ------------------------------------------------------
	 */

	/**
	 * @param integer $numberOfSpace
	 */
	function generateNoneBreakSpace( $numberOfSpace)
	{
		$spaceChar = "&nbsp;";
		for (
			$spaceCount = 1; // first star count
			$spaceCount <= $numberOfSpace; // checking star count if in range if numberOfStar
			$spaceCount ++ // increase start count
		) {
			echo $spaceChar;
		}
	}


	/** @var integer $maxLine Number of lines to be printed */
	$maxLine = 10; // maximum lines to print

	/** @var string $newLine this holds the new line character : PHP_EOL(php end of line) is a php constant and is same as \n character */
	$newLine = "<br>" . PHP_EOL;


	/**
	 * -----------------------------
	 * Generating a RIGHT ANGLED LIKE stars
	 * ---------------------------------------
	 */

	// This loop to print lines
	for (
		$line = 1; // initialize line 1.
		$line <= $maxLine; // checking line must be less than or equal to max line
		$line ++ // increase line by 1
	) {

		/** @var integer $numberOfStar our numbers of stars to print will be the current line number */
		$numberOfStar = $line;
		// run function to generate numberOfStar
		generateStar( $numberOfStar );
		// make the next $numberOfStar generate on new line by printing our saved new line character
		echo $newLine;
	}

	generateStar( 50, '-' );
	echo $newLine;


	/**
	 * -----------------------------
	 * Generating a PYRAMID LIKE stars
	 * ------------------------------------
	 */

	/** @var integer $spacePadding This is the number of space needed to push each line stars away */
	$numberOfSpace = $maxLine;

	// This loop to print lines
	for (
		$line = 1; // initialize line 1.
		$line <= $maxLine; // checking line must be less than or equal to max line
		$line ++ // increase line by 1
	) {

		/** @var integer $numberOfStar our numbers of stars to print will be the current line number */
		$numberOfStar = $line;
		// run function to generate numberOfSpace
		generateNoneBreakSpace( $numberOfSpace );
		// run function to generate numberOfStar
		generateStar( $numberOfStar );
		// make the next $numberOfStar generate on new line by printing our saved new line character
		echo $newLine;
		// decrease the number of spaces by 1
		$numberOfSpace--;
	}
